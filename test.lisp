(defvar *events->tickets* (list))
;; cars of list is event, cdrs are attendees. both are instances of the classes below.

(defclass ev ()
  ((name :initarg :name :initform nil)
   (date :initarg :date :initform nil)))

(defclass attendee ()
  ((first-name :initarg :first-name :initform nil)
   (last-name :initarg :last-name :initform nil)
   (email :initarg :email :initform nil)))

(defun event-equalp (event name date)
  (and (equalp name (slot-value event 'name))
       (equalp date (slot-value event 'date))))

(defun cancel-tickets (&key event date)
  (setq *events->tickets*
        (delete-if (lambda (entry) (event-equalp (car entry) event date))
                   *events->tickets*)))

(defun add-event (&key name date)
  (push (list (make-instance 'ev :name name :date date)) *events->tickets*))

(defun add-ticket (&key event date first-name last-name email)
  (let ((entry (find-if (lambda (e) (event-equalp (car e) event date))
                        *events->tickets*)))
    (if entry
        (nconc entry (cons (make-instance 'attendee
                                          :first-name first-name
                                          :last-name last-name
                                          :email email) nil))
        (error "no event ~a on ~a" event date))))

(defun example-starter ()
  (add-event :name "Example event"
             :date "10/10/22")
  (add-ticket
   :event "Example event"
   :date "10/10/22" 
   :first-name "John" :last-name "Smith"
   :email "example@email.com"))

(example-starter)

(defun generate-view-events-dom ()
  (cl-naive-dom:with-dom
    (:html
     (:head
      (:title "Events"))
     (:body
      (:div
       (:h1 "Events")
       (:div
        (loop for entry in *events->tickets*
              collect
              (with-slots (name date) (car entry)
                (list
                 (:div
                  (:p name)
                  (:p date))
                 (:div
                  (:p "Attendees")
                  (loop for attendees in (cdr entry)
                        collect
                        (with-slots (first-name last-name email) attendees 
                          (list
                           (:div
                            (:p "First name")
                            (:p first-name)
                            (:p "Last name")
                            (:p last-name)
                            (:p "Email")
                            (:p email)))))))))))))))

(defun generate-view-events-html ()
  (cl-naive-dom.abt:with-abt (:type :abt)
    (generate-view-events-dom)))

(print (generate-view-events-html))
